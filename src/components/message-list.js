import React, { Component } from 'react';
import Api from '../api';

class MessageList extends Component {
  constructor(...args) {
    super(...args);
    this.state = {
      messages: [],
    };
  }
  componentWillMount() {
    this.api = new Api({
      messageCallback: (message) => {
        this.messageCallback(message);
      },
    });
  }
  componentDidMount() {
    this.api.start();
  }
  messageCallback(message) {
    this.setState({
      messages: [
        ...this.state.messages.slice(),
        message
      ]
    }, () => {
      console.log(this.state.messages);
    });
  }
  renderButton() {
    const isApiStarted = this.api.isStarted();
    return (
      <button onClick={() => {
        if (isApiStarted) {
          this.api.stop();
        } else {
          this.api.start();
        }
        this.forceUpdate();
      }}
      >{isApiStarted ? 'Stop Messages' : 'Start Messages'}</button>
    );
  }
  render() {
    return (
      <div>
        {this.renderButton()}
      </div>
    );
  }
}

export default MessageList;
